use anyhow::Result;

use std::env;
use std::fs;
use std::fs::{File,Metadata};
use std::io::Write;
use std::time::{Instant, SystemTime};

use std::iter;
use std::str::Chars;

use std::str;
use std::process::Command;

#[derive(PartialEq, Clone)]
enum PrintType {
    StdOut, StdErr, AllOutputs
}

fn com(print: &PrintType, command_text: &str) -> String {
    let command_output = Command::new("bash").arg("-c")
        .arg(command_text)
        .output().unwrap();

    let to_string = |bytes| str::from_utf8(bytes).unwrap().to_string();
    let out = to_string(&command_output.stdout);
    let err = to_string(&command_output.stderr);

    match print {
        PrintType::StdOut => { print!("{out}"); out },
        PrintType::StdErr => { print!("{err}"); err },
        PrintType::AllOutputs => {
            print!("{out}");
            if err.trim().is_empty() {
                print!("{err}");
            }
            out
        },
    }
}

fn earlier_than(earlier: SystemTime, later: SystemTime) -> bool {
    earlier.duration_since(later).is_ok()
}

fn before(predicate: &Result<Metadata, std::io::Error>, comparison: &Result<Metadata, std::io::Error>) -> bool {
    if let (Ok(comparison), Ok(predicate)) = (comparison, predicate) {
        let sm = comparison.modified();
        let cm = predicate.modified();
        if let (Ok(script_mod_time), Ok(compiled_mod_time)) = (sm, cm) {
            if earlier_than(compiled_mod_time, script_mod_time) {
                return true;
            }
        }
    }
    false
}

fn needs_compilation(script_name: &str) -> bool {
    if cfg!(debug_assertions) {
        return true;
    }
    let script_attr = fs::metadata(script_name);
    let compiled_attr = fs::metadata(output_filename(script_name));
    let executable_attr = fs::metadata(std::env::args().next().unwrap());

    if before(&compiled_attr, &script_attr) ||
        before(&compiled_attr, &executable_attr) {
        false
    } else {
        true
    }
}
fn output_filename(script_name: &str) -> String {
    let (dir, base_filename) = script_name.rsplit_once('/').unwrap();
    format!("{dir}/.{base_filename}.compiled")
}

fn run_compiled(script_name: &str) {
    com(&PrintType::AllOutputs, &output_filename(script_name));
}

fn contains<T: std::cmp::PartialEq>(opt: Option<T>, value: T) -> bool {
    opt.is_some() && opt.unwrap() == value
}

fn parse_bash_command(
    iter: &mut iter::Peekable<Chars>,
    code: &mut String,
    is_expr: bool
) -> Result<()> {
    let next = iter.peek().expect("Unexpected EOF");
    code.push_str(&(match next {
        '^' => {
            iter.next();
            format!("out_and_err_com({}, \"", !is_expr)
        },
        '?' => {
            iter.next();
            format!("stderr_com({}, \"", !is_expr)
        },
        _ => format!("stdout_com({}, \"", !is_expr)
    }));
    while let Some(c) = iter.next() {
        if c == '%' {
            // Double percents count as a single literal percent
            if contains(iter.peek(), &'%') {
                iter.next();
            } else {
                break;
            }
        }
        if c == '\\' {
            code.push('\\');
        }
        code.push(c);
    }
    code.push_str("\")");
    let peek = iter.peek();
    if !is_expr && (peek.is_none() || contains(peek, &' ') || contains(peek, &'\n')) {
        code.push(';');
    }
    Ok(())
}

fn collect_code(script_name: &str) -> Result<String> {
    let script = fs::read_to_string(script_name)?;

    let mut code =
"#![allow(dead_code)]

use std::str;
use std::process::Command;

fn stdout_com(print: bool, command_text: &str) -> String {
    let s = str::from_utf8(&Command::new(\"bash\").arg(\"-c\")
        .arg(command_text).output().unwrap().stdout).unwrap().to_string();
    if print {
        print!(\"{}\", s)
    };
    return s;
}

fn stderr_com(print: bool, command_text: &str) -> String {
    let s = str::from_utf8(&Command::new(\"bash\").arg(\"-c\")
        .arg(command_text).output().unwrap().stderr).unwrap().to_string();
    if print {
        print!(\"{}\", s)
    };
    return s;
}

fn out_and_err_com(print: bool, command_text: &str) -> (String, String) {
    let output = Command::new(\"bash\").arg(\"-c\")
        .arg(command_text).output().unwrap();
    let stdout = str::from_utf8(&output.stdout).unwrap().to_string();
    let stderr = str::from_utf8(&output.stderr).unwrap().to_string();
    if print {
        print!(\"{}\", stdout);
        eprint!(\"{}\", stderr);
    };
    return (stdout, stderr);
}

fn main() -> std::io::Result<()> {
".to_string();


    let mut iter = script.chars().peekable();
    if script.starts_with("#!/") {
        while let Some(c) = iter.next() {
            if c == '\n' {
                break;
            }
        }
    }

    let mut is_expr = false;
    while let Some(c) = iter.next() {
        if c == '%' {
            // Double percents count as a single literal percent
            if contains(iter.peek(), &'%') {
                code.push('%');
                iter.next();
            } else {
                parse_bash_command(&mut iter, &mut code, is_expr)?;
            }
        } else {
            code.push(c);
            if (c == 'i' && contains(iter.peek(), &'f'))
            || (c == '=' && !contains(iter.peek(), &'=')) {
                is_expr = true;
            } else if c == 'f' && contains(iter.peek(), &'o') {
                iter.next();
                code.push('o');
                if contains(iter.peek(), &'r') {
                    is_expr = true;
                }
            } else if (c == '{' || c == ';') && is_expr {
                is_expr = false;
            } else if c == '\n' && iter.peek().is_some() {
                code.push_str("    ");
            }
        }
    }
    code.push_str("    Ok(())\n}");
    Ok(code)
}

fn main() -> Result<()> {
    let now = Instant::now();
    if let Some(script_name) = env::args().nth(1) {
        if !needs_compilation(&script_name) {
            run_compiled(&script_name);
            return Ok(());
        }

        let script_base_filename = script_name.split('/').last().unwrap();
        let code = collect_code(&script_name)?;
        let code_filename = format!("/tmp/{}-rush.rs",
            script_base_filename.replace('.', "-"));
        File::create(&code_filename)?.write_all(code.as_bytes())?;

        if cfg!(debug_assertions) {
            println!("[Start Rust]");
            println!("{code}");
            println!("[End Rust]");
        }

        let compile_command = format!(
            "rustc {} -o {}",
            code_filename,
            output_filename(&script_name)
        );
        com(&PrintType::StdErr, &compile_command);
        println!("[32m[Compiled in {}ms][0m\n", now.elapsed().as_millis());
        run_compiled(&script_name);
    }
    Ok(())
}
