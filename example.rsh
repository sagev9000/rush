#!/bin/rush

let x = %cat example.rsh%;

if x.contains("example") {
    %echo It\'s a script!%
}

if %echo some words%.contains("me wo") {
    println!("But it's also Rust!");
}

let mem = %free -h%.lines().nth(1).unwrap()
    .split_whitespace().nth(3).unwrap().to_owned();

println!("There are {} of memory free", &mem);

let (stdout, stderr) = %^curl bob%;
println!("Collect stdout and stderr separately!");
println!("  STDOUT:\n{}", stdout);
println!("  STDERR:\n{}", stderr);

// for i in 0..10000 {
//     println!("x contains %% example? [{}]", x.contains("example"));
// }

%echo Double-up your percent signs to display them literally 100%%!%

%
echo Can you write multi-line scripts?
echo Sure can! Even weird bash-isms:
for i in {0..4}; do
    echo $i
done
%

